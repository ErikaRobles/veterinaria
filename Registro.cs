﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace VeterinariaFinal
{
    public partial class Registro : Form
    {
        string connectionString = @"Server=localhost;Database=veterinariapruebadb;uid=root;pwd=E1r9MySQL#0;";
        int MascotaID = 0;
        public Registro()
        {
            InitializeComponent();
        }

        private void buttonRegistrar_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlCommand mySqlCmd = new MySqlCommand("MascotaAddOrEdit", mysqlCon);
                mySqlCmd.CommandType = CommandType.StoredProcedure;
                mySqlCmd.Parameters.AddWithValue("_MascotaID", MascotaID);
                mySqlCmd.Parameters.AddWithValue("_NPoseedor", textPoseedor.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_NMascota", textBox1.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_Raza", textBox2.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_Edad", textBox3.Text.Trim());
                mySqlCmd.ExecuteNonQuery();
                MessageBox.Show("se añadió correctamente");
                GridFill();
                Clear();
            }
        }
        void GridFill()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlDataAdapter sqlDa = new MySqlDataAdapter("MascotaViewAll", mysqlCon);
                sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dtblverinaria = new DataTable();
                sqlDa.Fill(dtblverinaria);
                dataGridViewRegistro.DataSource = dtblverinaria;
                dataGridViewRegistro.Columns[0].Visible = false;
            }
        }

        void Clear()
        {
            textPoseedor.Text = textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text= "";
            MascotaID = 0;
            buttonRegistrar.Text = "Guardar";
            buttonBorrar.Enabled = false;
        }

        private void Registro_Load(object sender, EventArgs e)
        {
            GridFill();
            Clear();
        }

        private void dataGridViewRegistro_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridViewRegistro.CurrentRow.Index != -1)
            {
                textPoseedor.Text = dataGridViewRegistro.CurrentRow.Cells[1].Value.ToString();
                textBox1.Text = dataGridViewRegistro.CurrentRow.Cells[2].Value.ToString();
                textBox2.Text = dataGridViewRegistro.CurrentRow.Cells[3].Value.ToString();
                textBox3.Text = dataGridViewRegistro.CurrentRow.Cells[4].Value.ToString();
                MascotaID = Convert.ToInt32(dataGridViewRegistro.CurrentRow.Cells[0].Value.ToString());
                buttonRegistrar.Text = "Guardar";
                buttonBorrar.Enabled = Enabled;
            }
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlDataAdapter sqlDa = new MySqlDataAdapter("MascotaSeachbyValor", mysqlCon);
                sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDa.SelectCommand.Parameters.AddWithValue("_BuscarValor", textBox4.Text);
                DataTable dtblverinaria = new DataTable();
                sqlDa.Fill(dtblverinaria);
                dataGridViewRegistro.DataSource = dtblverinaria;
                dataGridViewRegistro.Columns[0].Visible = false;
            }
        }

        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlCommand mySqlCmd = new MySqlCommand("MascotaDeleteByID", mysqlCon);
                mySqlCmd.CommandType = CommandType.StoredProcedure;
                mySqlCmd.Parameters.AddWithValue("_MascotaID", MascotaID);
                mySqlCmd.ExecuteNonQuery();
                MessageBox.Show("se eliminó correctamente");
                GridFill();
                Clear();
            }
        }
    }
}
